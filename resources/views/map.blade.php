@extends('layouts.app')

@section('content')
    <div id="mapid">

    </div>
@endsection

@section('script')
    <script>
        var mymap = L.map('mapid').setView([6.927079, 79.861244], 13);

        let markers = [];
        let polylines = [];
        let counter = 0;
        const colors = ['#FF0000', '#65FF00', '#00FFF9', '#BA00FF', '#FF9A1B'];

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoidGhhcnN1cCIsImEiOiJjajh3bGR6dnMwOXF1MzJucnR3dnNvNDdvIn0.yhs_AtObfvvgWXtPWFJAxA', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1IjoidGhhcnN1cCIsImEiOiJjajh3bGR6dnMwOXF1MzJucnR3dnNvNDdvIn0.yhs_AtObfvvgWXtPWFJAxA'
        }).addTo(mymap);

        $.ajax({
            type:'GET',
            url:'<?php echo \Illuminate\Support\Facades\URL::to('/'); ?>/api/text-messages',
            data:'_token = <?php echo csrf_token() ?>',
            success:function(response){
                setData(response);
            }
        });

        setInterval(function(){
            $.ajax({
                type:'GET',
                url:'<?php echo \Illuminate\Support\Facades\URL::to('/'); ?>/api/text-messages',
                data:'_token = <?php echo csrf_token() ?>',
                success:function(response){
                    setData(response);
                }
            });
        }, 30000);

        function setData(data){

            clearMap();

            setMarkers(data);

        }

        function setMarkers(data){

            counter = 0;

            for (let imeiData in data){
                markers = [];

                for(let i=0; i<data[imeiData].length;i++){

                    let marker = L.marker([data[imeiData][i].latitude, data[imeiData][i].longitude]).addTo(mymap);
                    let formattedMsg = data[imeiData][i].message.replace(/(?:\r\n|\r|\n)/g, '<br>');

                    let popupText = "<b>Boat No: </b>"+data[imeiData][i].imei+
                        "<br><b>True Heading: </b>"+data[imeiData][i].heading+
                        "<br><b>Speed: </b>"+data[imeiData][i].speed+
                        "<br><b>Latitude: </b>"+data[imeiData][i].latitude+
                        "<br><b>Longitude: </b>"+data[imeiData][i].longitude+
                        "<br><b>Timestamp: </b>"+data[imeiData][i].date_time+
                        "<br><hr><b>Message</b><br>"
                        +formattedMsg+"<br>";


                    let popup = L.popup({
                        closeOnClick: false,
                        minWidth:200
                    }).setContent(popupText);

                    marker.bindPopup(popup);

                    markers.unshift(marker);

                    if((i+1) !== data[imeiData].length){
                        let currentMarkerPoint = new L.LatLng(data[imeiData][i].latitude, data[imeiData][i].longitude);
                        let nextMarkerPoint = new L.LatLng(data[imeiData][i+1].latitude, data[imeiData][i+1].longitude);
                        let points = [currentMarkerPoint,nextMarkerPoint];

                        let polyline = L.polyline(points, {color: colors[counter]}).addTo(mymap);
                        polylines.unshift(polyline);
                    }

                }

                counter++;
            }

        }

        function clearMap() {

            for(let i in markers) {
                mymap.removeLayer(markers[i]);
            }

            for(let i in polylines) {
                mymap.removeLayer(polylines[i]);
            }
        }
    </script>
@endsection

@section('style')
    <style>
        #mapid { height: 100%; }
    </style>
@endsection