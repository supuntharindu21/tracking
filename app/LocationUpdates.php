<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationUpdates extends Model
{
    protected $table = 'location_updates';

    public $timestamps = false;

    protected $fillable = ['imei','latitude', 'longitude', 'heading', 'speed'];
}
