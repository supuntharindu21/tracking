<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LastLocationUpdate extends Model
{
    protected $table = 'last_location_update';

    public $timestamps = false;

    protected $fillable = ['imei','latitude', 'longitude', 'heading', 'speed'];
}
