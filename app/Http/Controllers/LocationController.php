<?php

namespace App\Http\Controllers;

use App\LastLocationUpdate;
use App\LocationUpdates;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    public function locations(Request $request)
    {

        $data = $request->all();

        LocationUpdates::create($data);

        $noOfRecs = LastLocationUpdate::where('imei', $data['imei'])->count();

        if($noOfRecs == 0){
            LastLocationUpdate::create($data);
        }else{
            LastLocationUpdate::where('imei', $data['imei'])
                ->update($data);

        }

    }

    public function lastLocation(Request $request)
    {
        $data = $request->all();

        LastLocationUpdate::create($data);
    }
}
