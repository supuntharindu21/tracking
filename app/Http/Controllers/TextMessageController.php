<?php

namespace App\Http\Controllers;

use App\TextMessage;
use Illuminate\Http\Request;

class TextMessageController extends Controller
{

    public function index()
    {
        $texts = TextMessage::all()->groupBy("imei")->sortBy("id");

        return response()->json($texts);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        TextMessage::create($data);
    }

}
