<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TextMessage extends Model
{
    protected $table = 'text_messages';

    public $timestamps = false;

    protected $fillable = ['imei','latitude', 'longitude', 'heading', 'speed', 'message'];

}
